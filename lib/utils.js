const repeat = n => f => x => {
  if (n > 0) { return repeat(n - 1)(f)(f(x)) } else { return x }
}

const times = n => f =>
  repeat(n)(i => (f(i), i + 1))(0) /* eslint-disable-line */

function isFullyVisible (el) {
  var elementBoundary = el.getBoundingClientRect()

  var top = elementBoundary.top
  var bottom = elementBoundary.bottom

  return ((top >= 0) && (bottom <= window.innerHeight))
}

function isPartiallyVisible (el) {
  var elementBoundary = el.getBoundingClientRect()

  var top = elementBoundary.top
  var bottom = elementBoundary.bottom
  var height = elementBoundary.height

  return ((top + height >= 0) && (height + window.innerHeight >= bottom))
}

module.exports = {
  times,
  isPartiallyVisible,
  isFullyVisible
}
