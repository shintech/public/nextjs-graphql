const cacheableResponse = require('cacheable-response')
const { routes } = require('../routes')

const ssrCache = (app, server) => {
  const cache = cacheableResponse({
    ttl: 1000 * 60 * 60,
    get: async ({ req, res, pagePath, queryParams }) => ({
      data: await app.renderToHTML(req, res, pagePath, queryParams)
    }),
    send: ({ data, res }) => res.send(data)
  })

  routes.map(({ pattern, page }) =>
    server.get(pattern, (req, res) =>
      cache({
        req,
        res,
        pagePath: page,
        queryParams: (req.params.id) ? { id: req.params.id } : {}
      })
    )
  )
}

module.exports = ssrCache
