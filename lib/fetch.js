const fetchJSON = (url, query) => {
  return new Promise(function (resolve, reject) {
    const baseURL = new URL(url)
    baseURL.pathname = '/api'

    if (query) { baseURL.search = new URLSearchParams(query).toString() }

    const headers = {
      'Content-Type': 'application/json'
    }

    fetch(baseURL.href, {
      headers
    })
      .then(raw => raw.json())
      .then(json => resolve(json))
      .catch(err => reject(err))
  })
}

export default fetchJSON
