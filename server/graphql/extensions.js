const isProd = process.env['NODE_ENV'] === 'production'

class BasicLogging {
  constructor (logger) {
    this.logger = logger
  }

  requestDidStart ({ operationName, queryString, parsedQuery, variables, requestContext }) {
    const logger = this.logger

    logger.info(`graphql: ${operationName || 'Undefined Operation'}...`)

    if (!isProd) {
      const query = queryString || parsedQuery || requestContext.source

      if (query) { logger.info(`graphql: ${query}`) }
      if (variables) { logger.info(`graphql - variables: ${JSON.stringify(variables, null, 2)}\n`) }
    }
  }
}

module.exports = logger => [
  () => new BasicLogging(logger)
]
