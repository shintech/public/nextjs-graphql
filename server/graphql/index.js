const { ApolloServer } = require('apollo-server-express')
const { RedisCache } = require('apollo-server-cache-redis')
const typeDefs = require('./typedefs')
const resolvers = require('./resolvers')
const dataSources = require('./datasources')
const Extensions = require('./extensions')

const isProd = process.env['NODE_ENV'] === 'production'
const REDIS_HOST = process.env['REDIS_HOST'] || '127.0.0.1'
const ENGINE_API_KEY = process.env['ENGINE_API_KEY'] || 'service:Graph:apiKey'

const cache = new RedisCache({
  host: REDIS_HOST,
  port: 6379
})

module.exports = (logger) =>
  new ApolloServer({
    typeDefs,
    resolvers,
    dataSources,
    extensions: Extensions(logger),
    debug: !isProd,
    cache,
    cacheControl: { defaultMaxAge: 5 },

    persistedQueries: {
      cache
    },

    engine: {
      apiKey: ENGINE_API_KEY
    }
  })
