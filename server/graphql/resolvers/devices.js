module.exports = {
  Query: {
    devices: async (_, props, { dataSources }) => dataSources.devices.all(),
    device: async (_, props, { dataSources }) => dataSources.devices.fetchOneAndCache(props.id)
  },

  Mutation: {
    createDevice: async (_, props, { dataSources }) => dataSources.devices.create(props)
  }
}
