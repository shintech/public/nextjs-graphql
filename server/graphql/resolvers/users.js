module.exports = {
  Query: {
    users: async (_, props, { dataSources }) => dataSources.users.all(),
    user: async (_, props, { dataSources }) => dataSources.users.one(props.id)
  },

  Mutation: {
    createUser: async (_, props, { dataSources }) => dataSources.users.create(props)
  }
}
