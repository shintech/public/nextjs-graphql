const { DataSource } = require('apollo-datasource')

const isProd = process.env['NODE_ENV'] === 'production'

module.exports = class DataSourceTemplate extends DataSource {
  constructor (props) {
    super(props)
    this.model = props
  }

  initialize ({ cache }) {
    this.cache = cache
  }

  all () {
    try {
      return this.model.find().sort({ _id: 'desc' })
    } catch (err) {
      return new Error(err)
    }
  }

  async fetchOneAndCache (id) {
    const { client } = this.cache
    const key = 'model-' + id

    if (isProd) {
      const cached = await client.get(key)

      if (cached) {
        return JSON.parse(cached)
      }
    }

    const model = await this.model.findById(id)

    if (isProd) { client.set(key, JSON.stringify(model), 'EX', 1000 * 60 * 60) }

    return model
  }
}
