const DataSource = require('./template')

module.exports = class DevicesAPI extends DataSource {
  async create ({ device }) {
    const Model = this.model

    try {
      const model = new Model()

      model.set(device)

      await model.save()

      return model
    } catch (err) {
      return new Error(err)
    }
  }
}
