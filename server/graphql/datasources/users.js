const DataSource = require('./template')

module.exports = class UsersAPI extends DataSource {
  async create ({ user }) {
    const Model = this.model

    try {
      const model = new Model()

      model.set(user)

      await model.save()

      return model
    } catch (err) {
      return new Error(err)
    }
  }

  all () {
    try {
      return this.model.find().populate('device').sort({ _id: 'desc' })
    } catch (err) {
      return new Error(err)
    }
  }

  one (id) {
    try {
      return this.model.findById(id).populate('device')
    } catch (err) {
      return new Error(err)
    }
  }
}
