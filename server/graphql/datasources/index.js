const UsersAPI = require('./users')
const DevicesAPI = require('./devices')

const User = require('../../models/User')
const Device = require('../../models/Device')

module.exports = () => ({
  users: new UsersAPI(User),
  devices: new DevicesAPI(Device)
})
