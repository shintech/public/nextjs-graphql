const express = require('express')
const router = express.Router()

module.exports = function () {
  router.route('/info')
    .get((req, res) => {
      res.status(200)
        .json({
          message: 'success'
        })
    })

  router.use((req, res) => {
    res.status(400)
      .send('404 - Not Found')
  })

  return router
}
