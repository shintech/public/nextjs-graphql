/* eslint-env jest */

var request = require('supertest').agent(_server)

afterAll(async () => {
  await _server.close()
})

describe('SERVER -> GET -> /sitemap.xml...', () => {
  let res

  beforeAll(async () => {
    res = await request.get('/sitemap.xml')
  })

  it('expect res.status to be \'200\'...', () => {
    expect(res.status).toBe(200)
  })
})
