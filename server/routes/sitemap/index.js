const { SitemapStream, streamToPromise } = require('sitemap')
const { createGzip } = require('zlib')

let sitemap

function setupSiteMap (req, res) {
  const host = req.headers['host']
  const baseURL = `https://${host}`

  res.header('Content-Type', 'application/xml')
  res.header('Content-Encoding', 'gzip')

  if (sitemap) {
    res.send(sitemap)
    return
  }

  try {
    const smStream = new SitemapStream({ hostname: baseURL })
    const pipeline = smStream.pipe(createGzip())

    smStream.write({
      url: '/',
      changefreq: 'daily',
      priority: 1,
      lastmodISO: new Date().toISOString()
    })
    smStream.end()

    // cache the response
    streamToPromise(pipeline).then(sm => sitemap = sm) // eslint-disable-line
    // stream the response
    pipeline.pipe(res).on('error', (e) => { throw e })
  } catch (e) {
    console.error(e) // eslint-disable-line
    res.status(500).end()
  }
}

module.exports = setupSiteMap
