const Adapter = require('enzyme-adapter-react-16')
const jest = require('jest')
const { configure } = require('enzyme')
const nock = require('nock')
const { Logger } = require('shintech-utils')
const Server = require('../../server')
const Router = require('../../server/router')
const mock = require('./mock')
require('isomorphic-unfetch')

const NODE_ENV = process.env['NODE_ENV'] || 'test'
const PORT = process.env['PORT'] || 8000
const HOST = process.env['HOST'] || 'localhost'

global.process.env = {
  NODE_ENV,
  PORT,
  HOST
}

configure({ adapter: new Adapter() })

jest.mock('next/config', () => () => ({
  publicRuntimeConfig: {
    NODE_ENV: 'test',
    VALUE: 'value',
    siteName: 'siteName',
    theme: {}
  }
}))

nock('http://localhost')
  .get('/api')
  .reply(200, {
    loaded: true
  })

const logger = new Logger(mock.options)
const router = new Router()

const server = Server({ router })
  .set('logger', logger)
  .use('/api', router)
  .listen()

global._server = server
global._mock = mock
