/* eslint-disable */

db.createUser(
  {
    user: 'admin',
    pwd: 'staging_password',
    roles: [
      {
        role: 'readWrite',
        db: 'seed_production'
      }
    ]
  }
)

/* eslint-enable */
