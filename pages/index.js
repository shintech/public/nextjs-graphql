import React from 'react'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import { withApollo } from 'lib/client/WithApollo'
import { NetworkStatus } from 'apollo-client'
import { useQuery } from '@apollo/react-hooks'
import { Link } from 'routes'
import Layout from 'layouts/Main'
import Table from 'layouts/Table'
import Err from 'components/Error'
import Loading from 'components/Loading'

import QUERY from './query.gql'

function IndexPage ({ baseURL, router }) {
  const queryVars = {
    notifyOnNetworkStatusChange: true,
    errorPolicy: 'all'
  }

  const { loading, data, error, networkStatus } = useQuery(QUERY, queryVars)

  const loadingMore = networkStatus === NetworkStatus.fetchMore

  return (
    <Layout title='Home' canonical={`${baseURL}${router.asPath}`}>
      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Table headers={['ID', 'Name', 'Username']}>
            {data.users.map(user =>
              <tr key={user._id}>
                <td><Link route='singleUser' params={{ id: user._id }}><a>{user._id}</a></Link></td>
                <td>{user.fullName}</td>
                <td>{user.username}</td>
              </tr>
            )}
          </Table>
      }

      <hr />

      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Table headers={['ID', 'Model', 'Manufacturer', 'Description']}>
            {data.devices.map(device =>
              <tr key={device._id}>
                <td><Link route='singleDevice' params={{ id: device._id }}><a>{device._id}</a></Link></td>
                <td>{device.model}</td>
                <td>{device.manufacturer}</td>
                <td>{device.description}</td>
              </tr>
            )}
          </Table>
      }
    </Layout>
  )
}

IndexPage.propTypes = {
  baseURL: PropTypes.string,
  router: PropTypes.object
}

export default withApollo(withRouter(IndexPage))
