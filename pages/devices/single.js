import React from 'react'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import { withApollo } from 'lib/client/WithApollo'
import { NetworkStatus } from 'apollo-client'
import { useQuery } from '@apollo/react-hooks'
import Layout from 'layouts/Main'
import Card from 'layouts/Card'
import Err from 'components/Error'
import Loading from 'components/Loading'

import QUERY from './single.gql'

const SingleDevicePage = ({ id, baseURL, router }) => {
  const queryVars = {
    variables: { id },
    notifyOnNetworkStatusChange: true,
    errorPolicy: 'all'
  }

  const { loading, data, error, networkStatus } = useQuery(QUERY, queryVars)

  const loadingMore = networkStatus === NetworkStatus.fetchMore

  return (
    <Layout title='Device Info' canonical={`${baseURL}${router.asPath}`}>
      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Card header={`Device ID: ${data.device._id}`}>
            <ul>
              <li>Model: {data.device.model}</li>
              <li>Manufacturer: {data.device.manufacturer}</li>
              <li>Description: {data.device.description}</li>
            </ul>
          </Card>
      }
    </Layout>
  )
}

SingleDevicePage.getInitialProps = ({ query: { id } }) => ({ id })

SingleDevicePage.propTypes = {
  id: PropTypes.string,
  baseURL: PropTypes.string,
  router: PropTypes.object
}

export default withApollo(withRouter(SingleDevicePage))
