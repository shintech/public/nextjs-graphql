import React, { useState } from 'react'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import { withApollo } from 'lib/client/WithApollo'
import { NetworkStatus } from 'apollo-client'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Link } from 'routes'
import Layout from 'layouts/Main'
import Table from 'layouts/Table'
import Err from 'components/Error'
import Loading from 'components/Loading'

import QUERY from './query.gql'
import MUTATION from './mutation.gql'

const DevicesPage = ({ baseURL, router }) => {
  const queryVars = {
    notifyOnNetworkStatusChange: true,
    errorPolicy: 'all'
  }

  const { loading, data, error, networkStatus } = useQuery(QUERY, queryVars)

  const loadingMore = networkStatus === NetworkStatus.fetchMore

  const [inputs, setInputs] = useState({
    model: '',
    description: '',
    manufacturer: ''
  })

  const handleChange = e => setInputs({ ...inputs, [e.target.name]: e.target.value })

  const [createDevice] = useMutation(MUTATION, {
    optimisticResponse: {
      __typename: 'Mutation',
      createDevice: {
        __typename: 'Device',
        _id: ' ',
        ...inputs
      }
    },

    update (cache, { data: { createDevice } }) {
      const { devices } = cache.readQuery({ query: QUERY })

      cache.writeQuery({
        query: QUERY,
        data: { devices: [createDevice, ...devices] }
      })
    }
  })

  const handleSubmit = (e) => {
    e.preventDefault()

    createDevice({
      variables: {
        device: inputs
      }
    })
  }

  return (
    <Layout title='Devices' canonical={`${baseURL}${router.asPath}`}>
      <form onSubmit={handleSubmit}>
        <input type='text' name='model' onChange={handleChange} value={inputs.model} />
        <input type='text' name='manufacturer' onChange={handleChange} value={inputs.manufacturer} />
        <input type='text' name='description' onChange={handleChange} value={inputs.description} />

        <input type='submit' value='Submit' />
      </form>

      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Table headers={['ID', 'Model', 'Manufacturer', 'Description']}>
            {data.devices.map(device =>
              <tr key={device._id}>
                <td><Link route='singleDevice' params={{ id: device._id }}><a>{device._id}</a></Link></td>
                <td>{device.model}</td>
                <td>{device.manufacturer}</td>
                <td>{device.description}</td>
              </tr>
            )}
          </Table>
      }
    </Layout>
  )
}

DevicesPage.propTypes = {
  baseURL: PropTypes.string,
  router: PropTypes.object
}

export default withApollo(withRouter(DevicesPage))
