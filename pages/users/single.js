import React from 'react'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import { withApollo } from 'lib/client/WithApollo'
import { NetworkStatus } from 'apollo-client'
import { useQuery } from '@apollo/react-hooks'
import { Link } from 'routes'
import Layout from 'layouts/Main'
import Card from 'layouts/Card'
import Err from 'components/Error'
import Loading from 'components/Loading'

import QUERY from './single.gql'

const SingleUserPage = ({ id, baseURL, router }) => {
  const queryVars = {
    variables: { id },
    notifyOnNetworkStatusChange: true,
    errorPolicy: 'all'
  }

  const { loading, data, error, networkStatus } = useQuery(QUERY, queryVars)

  const loadingMore = networkStatus === NetworkStatus.fetchMore

  return (
    <Layout title='User Info' canonical={`${baseURL}${router.asPath}`}>
      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Card header={data.user.fullName}>
            <ul>
              <li>Name: {data.user.fullName}</li>
              <li>Username: {data.user.username}</li>

              {data.user.device &&
              <li>Device:
                <ul>
                  <li>ID: <Link route='singleDevice' params={{ id: data.user.device._id }}><a>{data.user.device._id}</a></Link></li>
                  <li>Manufacturer: {data.user.device.manufacturer}</li>
                  <li>Model: {data.user.device.model}</li>
                  <li>Description: {data.user.device.description}</li>
                </ul>
              </li>
              }
            </ul>
          </Card>
      }
    </Layout>
  )
}

SingleUserPage.getInitialProps = ({ query: { id } }) => ({ id })

SingleUserPage.propTypes = {
  id: PropTypes.string,
  baseURL: PropTypes.string,
  router: PropTypes.object
}

export default withApollo(withRouter(SingleUserPage))
