import React, { useState } from 'react'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import { withApollo } from 'lib/client/WithApollo'
import { NetworkStatus } from 'apollo-client'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Link } from 'routes'
import Layout from 'layouts/Main'
import Table from 'layouts/Table'
import Err from 'components/Error'
import Loading from 'components/Loading'

import QUERY from './query.gql'
import MUTATION from './mutation.gql'

const UsersPage = ({ baseURL, router }) => {
  const queryVars = {
    notifyOnNetworkStatusChange: true,
    errorPolicy: 'all'
  }

  const { loading, data, error, networkStatus } = useQuery(QUERY, queryVars)

  const loadingMore = networkStatus === NetworkStatus.fetchMore

  const [inputs, setInputs] = useState({
    firstName: '',
    lastName: '',
    username: '',
    password: ''
  })

  const handleChange = e => setInputs({ ...inputs, [e.target.name]: e.target.value })

  const [createUser] = useMutation(MUTATION, {
    optimisticResponse: {
      __typename: 'Mutation',
      createUser: {
        __typename: 'User',
        _id: ' ',
        fullName: `${inputs.firstName} ${inputs.lastName}`,
        username: inputs.username
      }
    },

    update (cache, { data: { createUser } }) {
      const { users } = cache.readQuery({ query: QUERY })

      cache.writeQuery({
        query: QUERY,
        data: { users: [createUser, ...users] }
      })
    }
  })

  const handleSubmit = (e) => {
    e.preventDefault()

    createUser({
      variables: {
        user: inputs
      }
    })
  }

  return (
    <Layout title='Users' canonical={`${baseURL}${router.asPath}`}>
      <form onSubmit={handleSubmit}>
        <input type='text' name='firstName' onChange={handleChange} value={inputs.firstName} />
        <input type='text' name='lastName' onChange={handleChange} value={inputs.lastName} />
        <input type='text' name='username' onChange={handleChange} value={inputs.username} />
        <input type='password' name='password' onChange={handleChange} value={inputs.password} />

        <input type='submit' value='Submit' />
      </form>

      {(loading && !loadingMore) ? <Loading />
        : error ? <Err error={error.toString()}/>
          : <Table headers={['ID', 'Name', 'Username']}>
            {data.users.map(user =>
              <tr key={user._id}>
                <td><Link route='singleUser' params={{ id: user._id }}><a>{user._id}</a></Link></td>
                <td>{user.fullName}</td>
                <td>{user.username}</td>
              </tr>
            )}
          </Table>
      }
    </Layout>
  )
}

UsersPage.propTypes = {
  baseURL: PropTypes.string,
  router: PropTypes.object
}

export default withApollo(withRouter(UsersPage))
