const webpack = require('webpack')
const path = require('path')
const withBundleAnalyzer = require('@zeit/next-bundle-analyzer')
const withSass = require('@zeit/next-sass')
const runtime = require('./config')
const isProd = process.env['NODE_ENV'] === 'production'

const fileLoader = {
  test: /\.ico/,
  loader: 'file-loader',
  options: {
    context: '',
    emitFile: true,
    name: '[path][name].[ext]'
  }
}

// const gqlLoader = {
//   test: /\.(graphql|gql)$/,
//   exclude: /node_modules/,
//   loader: 'graphql-tag/loader'
// }

const eslintLoader = !isProd ? {
  enforce: 'pre',
  test: /\.js$/,
  exclude: /node_modules/,
  loader: 'eslint-loader',
  options: {
    failOnWarning: false,
    failOnError: false,
    emitWarning: true
  }
} : {}

const urlLoader = {
  loader: 'url-loader',
  options: {
    limit: 8192
  }
}

const imageLoader = {
  loader: 'image-webpack-loader',
  options: {
    enforce: 'pre'
  }
}

const config = {
  serverRuntimeConfig: runtime,
  publicRuntimeConfig: runtime,

  analyzeServer: ['server', 'both'].includes(process.env.BUNDLE_ANALYZE),
  analyzeBrowser: ['browser', 'both'].includes(process.env.BUNDLE_ANALYZE),

  bundleAnalyzerConfig: {
    server: {
      analyzerMode: 'static',
      reportFilename: path.join(__dirname, 'static', 'bundles', 'server.html')
    },
    browser: {
      analyzerMode: 'static',
      reportFilename: path.join(__dirname, 'static', 'bundles', 'client.html')
    }
  },

  webpack: (config, { dev }) => {
    config.plugins = config.plugins || []
    config.module.rules = config.module.rules || []

    config.plugins = [
      ...config.plugins,

      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env['NODE_ENV'])
      })
    ]

    config.module.rules = [
      ...config.module.rules,
      eslintLoader,
      fileLoader,
      // gqlLoader,
      {
        test: /\.(txt|jpg|png)$/,
        use: [
          imageLoader,
          urlLoader
        ]
      },

      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      }
    ]

    return config
  }
}

module.exports = withSass(withBundleAnalyzer(config))
