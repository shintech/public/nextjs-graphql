/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Component from 'components/Link'

afterAll(async () => {
  await _server.close()
})

// describe('COMPONENT -> Link -> Mock properties...', () => {
//   let component = shallow(<Component />)

//   it(`expect content header text to equal "${_mock.home.title}"...`, () => {
//     expect(component.find('.page-title').text()).toBe(`${_mock.home.title}`)
//   })
// })

describe('COMPONENT -> Link -> snapshot...', () => {
  const component = shallow(<Component />)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})
