import React from 'react'
// import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const LoadingComponent = () => (
  <Wrapper className='loader' />
)

LoadingComponent.propTypes = {}
LoadingComponent.defaultProps = {}

export default LoadingComponent
