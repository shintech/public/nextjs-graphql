/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Component from 'components/Loading'

afterAll(async () => {
  await _server.close()
})

// describe('COMPONENT -> Loading...', () => {
//   let component = shallow(<Component type='text' title='text Loading' value='' field='title'/>)

//   it(`expect label text to be "text Loading"...`, () => {
//     expect(component.find('.label span').text()).toBe('text Loading')
//   })

//   it(`expect to render text Loading...`, () => {
//     expect(component.find('Loading').props().type).toBe('text')
//   })
// })

describe('COMPONENT -> Loading -> snapshot...', () => {
  let component = shallow(<Component />)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})
