import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const InputComponent = ({ type, title, value, field, options, handleChange, ...etc }) =>
  <Wrapper className='input-field'>
    <label className='label' htmlFor={`${field}-text-input` }>
      <span>{ title }</span>
    </label>

    <br/>
    {(!type || type === 'text') &&
      <input id={ `${field}-text-input` } type='text' value={ value } onChange={ e => handleChange(field, e.target) } { ...etc }/>
    }

    {(type === 'select') &&
      <select value={ value } onChange={ e => handleChange(field, e.target) } { ...etc }>
        <option value="" disabled>Select an option...</option>

        {options.map((f, v) => <option key={ f } value={ f }>{ f }</option>)}
      </select>
    }
  </Wrapper>

InputComponent.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,
  field: PropTypes.string,
  options: PropTypes.array,
  handleChange: PropTypes.func
}

InputComponent.defaultProps = {
  handleChange: (e) => console.info(`New value : ${e.target.value}`), // eslint-disable-line
  field: null,
  title: 'default',
  value: ''
}

export default InputComponent
