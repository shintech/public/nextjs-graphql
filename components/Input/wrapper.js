import styled from 'styled-components'

const Wrapper = styled.p`
  label {
    text-transform: capitalize;
  }
`

export default Wrapper
