import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const ErrorComponent = ({ error }) => (
  <Wrapper>
    <h1>{error}</h1>
  </Wrapper>
)

ErrorComponent.propTypes = {
  error: PropTypes.string
}

ErrorComponent.defaultProps = {}

export default ErrorComponent
