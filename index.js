const http = require('http')
const mongoose = require('mongoose')
const { Logger } = require('shintech-utils')
const Server = require('./server')
const Router = require('./server/router')
const Apollo = require('./server/graphql')
const Cache = require('./lib/cache')
const nextRoutes = require('./routes')

const NAME = process.env['npm_package_name'] || 'app-name'
const VERSION = process.env['npm_package_version'] || '0.0.1'
const PORT = parseInt(process.env['PORT']) || 8000
const HOST = process.env['HOST'] || '0.0.0.0'
const NODE_ENV = process.env['NODE_ENV'] || 'development'
const MONGO_URI = process.env['MONGO_URI'] || 'mongodb://localhost/development'

const isProd = NODE_ENV === 'production'

const nextApp = require('next')({ dev: !isProd })
const handler = nextRoutes.getRequestHandler(nextApp)

const logger = new Logger()

const dbOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  connectTimeoutMS: 10000,
  user: process.env['MONGO_USER'],
  pass: process.env['MONGO_PASSWORD']
}

mongoose.connection.on('error', err => logger.error(err))

nextApp.prepare()
  .then(async () => {
    logger.info(`starting -> ${NAME} - version: ${VERSION}...`)

    try {
      await mongoose.connect(`${MONGO_URI}?authSource=admin`, dbOptions)

      logger.info(`connected to ${MONGO_URI}...`)
    } catch (err) {
      logger.error(err.message)
      throw err
    }

    const app = Server()
    const server = http.Server(app)
    const router = new Router()
    const apollo = Apollo(logger)

    apollo.applyMiddleware({
      app,
      path: '/graphql'
    })

    app.set('logger', logger)

    if (isProd) { Cache(nextApp, app) }

    app.use('/api', router)

    app.all('*', (req, res) => handler(req, res))

    server.listen(PORT, HOST)
      .on('listening', () => logger.info(`listening at ${HOST}:${PORT}...`))
      .on('error', err => logger.error(err.message))
      .on('close', () => {
        mongoose.connection.close()
      })
  })
