const { invert, setLightness } = require('polished')

const props = {
  primary: process.env['PRIMARY_COLOR'] || 'white',
  secondary: process.env['SECONDARY_COLOR'] || 'purple',
  accent: process.env['ACCENT_COLOR'] || 'teal',
  active: process.env['ACTIVE_COLOR'] || 'turquoise'
}

const properties = Object.entries(props)

const theme = properties.reduce((obj, color) => {
  obj[color[0]] = {
    color: color[1],
    inverted: invert(color[1]),
    light: setLightness(0.7, color[1]),
    dark: setLightness(0.3, color[1])
  }

  return obj
}, {})

module.exports = {
  NODE_ENV: process.env['NODE_ENV'],
  GA_TRACKING_ID: process.env['GA_TRACKING_ID'],
  VALUE: process.env['npm_package_config_value'],
  siteName: process.env['SITENAME'] || 'SiteName',
  theme
}
