# shintech/public/nextjs-graphql

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
	a. [.env ](#env) <br />
	b. [Development ](#development) <br />
	c. [Production ](#production) <br />
	c. [git hooks ](#git-hooks)
3. [ Usage ](#usage) <br />
   a. [ Caching ](#caching)
   b. [ Layouts ](#layouts)

<a name="synopsis"></a>
### Synopsis

Next.JS + Express.JS
  
<a name="install"></a>
### Installation

    ./install.sh

<a name="env"></a>
#### Copy the config for the environment from config/env/[environment].env and edit as necessary.

    PORT=8000
    NODE_ENV=development

<a name="development"></a>
#### Development

    npm run dev

    # or

    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="git-hooks"></a>
#### git hooks
    ln -s /path/to/repo/config/hooks/hook /path/to/repo/.git/hooks/

<a name="usage"></a>
### Usage

<a name="Caching"></a>
#### caching

GraphQL routes can be added to the cache function in /lib/withApollo.js.

    const cache = new InMemoryCache({
      cacheRedirects: {
        Query: {
          device: (_, args) => toIdValue(cache.config.dataIdFromObject({ __typename: 'Device', _id: args.id })),
          user: (_, args) => toIdValue(cache.config.dataIdFromObject({ __typename: 'User', _id: args.id }))
        }
      }
    })

<a name="Layouts"></a>
#### Layouts

##### Table

    import Table from 'layouts/Table'
    
    <Table headers={['ID', 'Name', 'Email']}>
      <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Email</td>
      </tr>
    </Table>
