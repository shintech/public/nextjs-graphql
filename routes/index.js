const routes = require('next-routes')()

routes.add('home', '/', 'index')
routes.add('users', '/users', 'users')
routes.add('devices', '/devices', 'devices')
routes.add('singleDevice', '/devices/:id', 'devices/single')
routes.add('singleUser', '/users/:id', 'users/single')

module.exports = routes
