import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const TableLayout = ({ headers, children }) =>
  <Wrapper className='table'>
    <thead>
      <tr>
        {headers.map(header => <th key={header} className='is-secondary-dark'>{header}</th>)}
      </tr>
    </thead>
    <tbody>
      {children}
    </tbody>
  </Wrapper>

TableLayout.propTypes = {
  headers: PropTypes.array,
  children: PropTypes.node
}

TableLayout.defaultProps = {
  headers: []
}

export default TableLayout
