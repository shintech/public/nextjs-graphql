import styled from 'styled-components'

const Wrapper = styled.table`
  width: 100%;
  min-height: 25vh;
  background-color: grey;
  border-collapse: collapse;
  border-style: hidden;
  border-radius: 1ch;
  overflow: hidden;

  th, td {
    border: 1px solid black;
    padding: 1ch;
    text-align: left;
  }

  tr:nth-child(odd) {
    background-color: darkgrey
  }
`

export default Wrapper
