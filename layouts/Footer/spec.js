/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Footer from 'layouts/Footer'
import { routes } from 'routes'

afterAll(async () => {
  await _server.close()
})

// describe('COMPONENT -> Footer -> Mock properties...', () => {
//   let component = shallow(<Footer routes={ routes }/>)

//   component.find('.nav-item').forEach((link, v) => {
//     it(`expect .nav-item text to equal "${routes[v].name}"...`, () => {
//       expect(link.text()).toBe(routes[v].name)
//     })
//   })
// })

describe('COMPONENT -> Footer -> snapshot...', () => {
  const component = shallow(<Footer routes={ routes }/>)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})
