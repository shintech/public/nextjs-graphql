import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const Footer = ({ siteName }) =>
  <Wrapper className='footer is-primary-dark'>
    <h3>Footer</h3>
    <a href="#" className='copyright is-accent-text'>&copy; {siteName}</a>
  </Wrapper>

Footer.propTypes = {
  siteName: PropTypes.string
}

Footer.defaultProps = {
  siteName: 'company'
}

export default Footer
