import styled from 'styled-components'

const Wrapper = styled.footer`
  width: 100%;
  min-height: 12vh;
  margin-top: 1.25em;

  display: flex;
  align-items: flex-end;
  justify-content: space-between;

  h3 {
    margin: 0;
    padding: 0;
  }
  
  a {
    margin-right: 1ch;
    text-decoration: none;
  }
`

export default Wrapper
