import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  font-family: "Times New Roman", Times, serif;

  h1, h2, h3, h4 {font-family: sans-serif;}
  code, pre, tt, kbd {font-family: monospace;}
  p.signature {font-family: cursive;}
  .tagline {font-family: "Lucida Console", Monaco, monospace;}

  background:
  radial-gradient(black 15%, transparent 16%) 0 0,
  radial-gradient(black 15%, transparent 16%) 8px 8px,
  radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,
  radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;
  background-color:#282828;
  background-size:16px 16px;
  background-attachment: fixed;

  main {
    width: 96vw;
    position: relative;
    margin-top: 4em;
    min-height: 79.5vh;
  }

  .is-primary { background-color: ${props => props.primary.color}; }
  .is-primary-light { background-color: ${props => props.primary.light}; }
  .is-primary-dark { background-color: ${props => props.primary.dark}; }
  .is-primary-inverted { background-color: ${props => props.primary.inverted}; }

  .is-secondary { background-color: ${props => props.secondary.color}; }
  .is-secondary-light { background-color: ${props => props.secondary.light}; }
  .is-secondary-dark { background-color: ${props => props.secondary.dark}; }  
  .is-secondary-inverted { background-color: ${props => props.secondary.light}; }

  .is-accent { background-color: ${props => props.accent.color}; }
  .is-accent-text { color: ${props => props.accent.color}; }
  .is-accent-inverted { background-color: ${props => props.accent.inverted}; }

  .is-active { background-color: ${props => props.active.color}; }
  .is-active-text { color: ${props => props.active.color}; }
  .is-active-inverted { background-color: ${props => props.active.inverted}; }
`

export default Wrapper
