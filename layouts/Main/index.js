import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'
import getConfig from 'next/config'
import { routes } from 'routes'
import Nav from 'layouts/Nav'
import Footer from 'layouts/Footer'
import Wrapper from './wrapper'

const { publicRuntimeConfig: { siteName, theme } } = getConfig()

const MainLayout = ({ title, description, canonical, children }) =>
  <Wrapper { ...theme } id='main-layout'>
    <Head>
      <title>{ title } | { siteName }</title>
      <meta name='description' content={ description } />
      <link rel='canonical' href={canonical} />
      <link id='favicon' rel='icon' href='/images/icon.ico' />
    </Head>

    <Nav routes={routes}/>

    <main>
      { children }
    </main>

    <Footer siteName={siteName}/>
  </Wrapper>

MainLayout.propTypes = {
  title: PropTypes.string,
  siteName: PropTypes.string,
  description: PropTypes.string,
  canonical: PropTypes.string,
  children: PropTypes.node
}

export default MainLayout
