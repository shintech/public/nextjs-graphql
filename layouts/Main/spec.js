/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Layout from 'layouts/Main'

afterAll(async () => {
  await _server.close()
})

describe('LAYOUT -> Main -> Mock properties...', () => {
  let layout = shallow(
    <Layout title='title' canonical='http://localhost:3000'>
      <h1>Test</h1>
      <p>Test</p>
    </Layout>
  )

  it(`expect title text to equal "tite | siteName"...`, () => {
    expect(layout.find('title').text()).toBe(`title | siteName`)
  })

  it(`expect to render "<main>"...`, () => {
    expect(layout.find('main').exists()).toEqual(true)
  })
})

describe('LAYOUT -> Main -> snapshot...', () => {
  it('expect to render correct properties', () => {
    let layout = shallow(
      <Layout title='title' siteName='siteName'>
        <h1>Test</h1>
        <p>Test</p>
      </Layout>
    )

    expect(layout).toMatchSnapshot()
  })
})
