import styled from 'styled-components'

const Wrapper = styled.form`
  background-color: ghostwhite;
  padding: 1em;
  border-radius: 0.5em;`

export default Wrapper
