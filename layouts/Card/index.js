import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const CardLayout = ({ header, children }) =>
  <Wrapper className='card'>
    <h3>{header}</h3>
    { children }
  </Wrapper>

CardLayout.propTypes = {
  header: PropTypes.string,
  children: PropTypes.node
}

CardLayout.defaultProps = {
  headers: 'Default Header'
}

export default CardLayout
