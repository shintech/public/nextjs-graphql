/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Layout from 'layouts/Card'

afterAll(async () => {
  await _server.close()
})

describe('LAYOUT -> Card -> Render...', () => {
  let layout = shallow(
    <Layout header='title'>
      <p>Test1</p>
    </Layout>
  )

  it(`expect header text to be "title"...`, () => {
    expect(layout.find('h3').text()).toBe('title')
  })
})

describe('LAYOUT -> Card -> snapshot...', () => {
  let layout = shallow(<Layout loaded={true} />)

  it('expect to render correct properties', () => {
    expect(layout).toMatchSnapshot()
  })
})
