import styled from 'styled-components'

const Wrapper = styled.nav`
  display: flex;
  align-items: center;
  position: fixed;
  height: 3em;
  top: 0;
  width: 100%;
  z-index: 1;

  .nav-item {
    margin: 1em;
    text-decoration: none;
    
    &.is-active-text {
      cursor: not-allowed;
    }
    
    &:hover {
      opacity: 0.8;
    }
  }
`

export default Wrapper
