import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'
import Link from 'components/Link'

const regex = /^\/(devices|users)\/([^/]+?)(?:\/)?$/i

const NavLayout = ({ routes }) =>
  <Wrapper className='navbar is-primary-inverted'>
    {routes.map((route, v) =>
      !route.pattern.match(regex) &&
      <Link key={v} route={route.page} activeClassName='is-active-text'>
        <a className='nav-item is-accent-text'>{route.name}</a>
      </Link>
    )}
  </Wrapper>

NavLayout.propTypes = {
  routes: PropTypes.array
}

NavLayout.defaultProps = {
  routes: []
}

export default NavLayout
