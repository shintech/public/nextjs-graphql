/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Nav from 'layouts/Nav'
import { routes } from 'routes'

global.window = {
  addEventListener: () => {}
}

afterAll(async () => {
  await _server.close()
})

// describe('COMPONENT -> Nav -> Mock properties...', () => {
//   let component = shallow(<Nav routes={ routes }/>)

//   component.find('.nav-item').forEach((link, v) => {
//     it(`expect .nav-item text to equal "${routes[v].name}"...`, () => {
//       expect(link.text()).toBe(routes[v].name)
//     })
//   })
// })

describe('COMPONENT -> Nav -> snapshot...', () => {
  const component = shallow(<Nav routes={ routes }/>)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})
